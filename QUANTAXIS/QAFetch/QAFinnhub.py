import datetime

import finnhub
import pandas as pd

from QUANTAXIS.QAUtil import QA_util_date_stamp, QA_util_time_stamp


class FinnhubFetcher:
    def __init__(self):
        # Setup client
        self.finnhub_client = finnhub.Client(api_key="c2ams0iad3ibqimof9h0")
        self.calender = pd.read_csv("../resources/us_trade_calender.csv")


    def fix_hour_err(self, x):
        if x.hour >= 12 and x.hour <= 18:
            return (x - datetime.timedelta(hours=24))
        else:
            return x

    def get_normal_data(self, input, code, _type):
        data = input
        data = data.assign(
            # datetime=pd.to_datetime(data['datetime'].apply(QA_util_future_to_us_realdatetime, 1), utc=False))
            datetime=pd.to_datetime(data['time_stamp'], unit='s'))

        data = data.assign(
            tradetime=data['datetime'].apply(lambda x: str(x)[0:16]),
            code=str(code),
            date=data['datetime'].apply(lambda x: str(x)[0:10]),
            date_stamp=data['datetime'].apply(
                lambda x: QA_util_date_stamp(x)),
            time_stamp=data['datetime'].apply(
                lambda x: QA_util_time_stamp(x)),
            type="{}min".format(_type)).set_index('datetime', drop=False, inplace=False)


        data = data.merge(self.calender, on=['date_stamp'], how='left')
        data = data[data['time_stamp'] <= data['end_timestamp']]
        data = data[data['time_stamp'] >= data['start_timestamp']]
        # return used columns and reset index. the sequence is very important, it will be used to gen 2, 4 day data
        data = (data[
            ['datetime', 'date', 'open', 'high', 'low', 'close', 'volume', 'time_stamp', 'date_stamp', "code", "type"]
        ]).reset_index(drop=True)

        return data

    def get_fix_data(self, input, code, _type):
        data = input
        data = data.assign(
            # datetime=pd.to_datetime(data['datetime'].apply(QA_util_future_to_us_realdatetime, 1), utc=False))
            datetime=pd.to_datetime(data['datetime']).apply(self.fix_hour_err))

        data = data.assign(
            tradetime=data['datetime'].apply(lambda x: str(x)[0:16]),
            code=str(code),
            date=data['datetime'].apply(lambda x: str(x)[0:10]),
            date_stamp=data['datetime'].apply(
                lambda x: QA_util_date_stamp(x)),
            time_stamp=data['datetime'].apply(
                lambda x: QA_util_time_stamp(x)),
            type="{}min".format(_type)).set_index('datetime', drop=False, inplace=False)

        return data

    def save_candle(self, code,
                            start_stamp,
                            end_stamp,
                            type,
                            frequence='5min'):
        # Stock candles
        if frequence == "1min":
            _type = "1"
        if frequence == "2min":
            _type = "2"
        if frequence == "5min":
            _type = "5"
        if frequence == "day":
            _type = "D"

        res = self.finnhub_client.stock_candles(code, _type, int(start_stamp), int(end_stamp))
        if res.get("s") and res.get("s") == "no_data":
            print("no data")
            return
        df = pd.DataFrame(res)
        data = df.rename(
            columns={"o": "open", "h": "high", "l": "low", "c": "close", "t": "time_stamp", "v": "volume"})

        data = data[data["time_stamp"] > start_stamp]
        data = data[data["time_stamp"] <= end_stamp]

        data["time_stamp"] = data["time_stamp"] - 18000

        data1 = self.get_normal_data(data, code, _type)
        data2 = self.get_fix_data(data1, code, _type)

        return data1, data2