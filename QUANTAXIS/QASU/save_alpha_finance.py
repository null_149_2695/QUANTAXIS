from time import sleep

import pymongo

import requests

from QUANTAXIS.QAUtil import QASETTING


class FinanceFetcher:
    def __init__(self):
        self.key = "9MOQ215XJGZJDVWR"
        self.client = QASETTING.client.quantaxis

    def fetch_income_statement(self, symbol):
        # replace the "demo" apikey below with your own key from https://www.alphavantage.co/support/#api-key
        url = 'https://www.alphavantage.co/query?function=INCOME_STATEMENT&symbol={}&apikey={}'.format(symbol, self.key)
        r = requests.get(url)
        data = r.json()
        coll = self.client.income_statement
        coll.create_index(
            [
                ('symbol',
                 pymongo.ASCENDING),
                ('fiscalDateEnding',
                 pymongo.ASCENDING)
            ]
        )
        annualReports = data.get("annualReports")
        if annualReports is None:
            print("income_statement: get {} data is none, return".format(symbol))
            return
        insert_array = []
        for item in annualReports:
            item['symbol'] = symbol
            fiscalDateEnding = item['fiscalDateEnding']
            insert_array.append(item)
            coll.delete_one({"symbol": symbol, "fiscalDateEnding": fiscalDateEnding})
        coll.insert_many(insert_array)

    def fetch_balance_sheet(self, symbol):
        url = 'https://www.alphavantage.co/query?function=BALANCE_SHEET&symbol={}&apikey={}'.format(symbol, self.key)
        # replace the "demo" apikey below with your own key from https://www.alphavantage.co/support/#api-key
        r = requests.get(url)
        data = r.json()
        coll = self.client.balance_sheet
        coll.create_index(
            [
                ('symbol',
                 pymongo.ASCENDING),
                ('fiscalDateEnding',
                 pymongo.ASCENDING)
            ]
        )
        annualReports = data.get("annualReports")
        if annualReports is None:
            print("balance_sheet: get {} data is none, return".format(symbol))
            return
        insert_array = []
        for item in annualReports:
            item['symbol'] = symbol
            fiscalDateEnding = item['fiscalDateEnding']
            insert_array.append(item)
            coll.delete_one({"symbol": symbol, "fiscalDateEnding": fiscalDateEnding})
        coll.insert_many(insert_array)

    def fetch_cash_flow(self, symbol):
        # replace the "demo" apikey below with your own key from https://www.alphavantage.co/support/#api-key
        url = 'https://www.alphavantage.co/query?function=CASH_FLOW&symbol={}&apikey={}'.format(symbol, self.key)
        # replace the "demo" apikey below with your own key from https://www.alphavantage.co/support/#api-key
        r = requests.get(url)
        data = r.json()
        coll = self.client.cash_flow
        coll.create_index(
            [
                ('symbol',
                 pymongo.ASCENDING),
                ('fiscalDateEnding',
                 pymongo.ASCENDING)
            ]
        )
        annualReports = data.get("annualReports")
        if annualReports is None:
            print("cash_flow: get {} data is none, return".format(symbol))
            return
        insert_array = []
        for item in annualReports:
            item['symbol'] = symbol
            fiscalDateEnding = item['fiscalDateEnding']
            insert_array.append(item)
            coll.delete_one({"symbol": symbol, "fiscalDateEnding": fiscalDateEnding})
        coll.insert_many(insert_array)


def save_finance():
    client = QASETTING.client.quantaxis
    coll = client.usstock_list

    ref_ = coll.find({}, {"code": 1})
    data = list(ref_)
    fetcher = FinanceFetcher()
    print("total size {}".format(len(data)))
    i = 1
    for item in data:
        try:
            code = item.get("code")
            print("{} fetching {}".format(i, code))
            fetcher.fetch_income_statement(code)
            sleep(10)
            fetcher.fetch_balance_sheet(code)
            sleep(10)
            fetcher.fetch_cash_flow(code)
            sleep(10)
        except Exception as e:
            print("error {}, info ={}".format(code, e))
        finally:
            i += 1


if __name__ == '__main__':
    fetcher = FinanceFetcher()
    code = "ZG"
    fetcher.fetch_income_statement(code)
    # sleep(10)
    fetcher.fetch_balance_sheet(code)
    # sleep(10)
    fetcher.fetch_cash_flow(code)
