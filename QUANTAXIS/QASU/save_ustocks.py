from time import sleep

from QUANTAXIS import QA_Setting
from QUANTAXIS.QASU.save_tdx import QA_SU_save_single_usstock_min, QA_SU_save_single_usstock_min_2

QASETTING = QA_Setting()
DATABASE = QASETTING.client.quantaxis


def get_stocks(client=DATABASE, ):
    coll = client.usstock_list

    ref_ = coll.find({}, {"code": 1})
    data = list(ref_)
    for item in data:
        try:
            code = item.get("code")
            if str(code).startswith("B") or str(code).startswith("A"):
                print("saving {}".format(code))
                QA_SU_save_single_usstock_min(code=code)
                sleep(5)
                # QA_SU_save_single_usstock_min_2(code=code)
                # sleep(2)
        except Exception as e:
            print("error {}, info ={}".format(code, e))


if __name__ == '__main__':
    get_stocks()
