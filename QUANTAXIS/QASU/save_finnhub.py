import datetime

import finnhub
import pandas as pd

from QUANTAXIS import QA_util_date_stamp, QA_util_time_stamp


class FinnhubSaver:

    def __init__(self):
        # Setup client
        self.finnhub_client = finnhub.Client(api_key="c2ams0iad3ibqimof9h0")

    def save_candle(self, code, _type, start_str, end_str):

        start = datetime.datetime.strptime(start_str, "%Y-%m-%d %H:%M:%S").timestamp()
        end = datetime.datetime.strptime(end_str, "%Y-%m-%d %H:%M:%S").timestamp()

        start_tmp = start
        end_tmp = start_tmp
        while start_tmp < end:
            end_tmp = end_tmp + 5270400
            if end_tmp > end:
                end_tmp = end
            # Stock candles
            res = self.finnhub_client.stock_candles(code, _type, start_tmp, end_tmp)
            if res.get("s") and res.get("s")  == "no_data":
                print("no data")
                return
            df = pd.DataFrame(res)
            data = df.rename(
                columns={"o": "open", "h": "high", "l": "low", "c": "close", "t": "time_stamp", "v": "volume"})
            data = data.assign(
                # datetime=pd.to_datetime(data['datetime'].apply(QA_util_future_to_us_realdatetime, 1), utc=False))
                datetime=pd.to_datetime(data['time_stamp'], unit='s'))

            data = data.assign(
                tradetime=data['datetime'].apply(lambda x: str(x)[0:16]),
                code=str(code),
                date=data['datetime'].apply(lambda x: str(x)[0:10]),
                date_stamp=data['datetime'].apply(
                    lambda x: QA_util_date_stamp(x)),
                time_stamp=data['datetime'].apply(
                    lambda x: QA_util_time_stamp(x)),
                type=_type).set_index('datetime', drop=False, inplace=False)
            data = data[data['time_stamp']>start_tmp]
            data = data[data['time_stamp'] <= end_tmp]
            print(data)

if __name__ == '__main__':
    code = "AAPL"
    _type = "5"

    FinnhubSaver().save_candle(code, _type)
