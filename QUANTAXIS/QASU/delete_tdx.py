import datetime

from QUANTAXIS import QA_Setting

QASETTING = QA_Setting()
DATABASE = QASETTING.client.quantaxis


def delete_data_one_week(code: str, client=DATABASE, ui_log=None):

    coll_stock_day = client.usstock_min
    start_date = datetime.datetime.strptime((datetime.datetime.now()-datetime.timedelta(days=14)).strftime("%Y-%m-%d"), "%Y-%m-%d").timestamp();
    myquery = {"code": code, "time_stamp": {"$gte": start_date}}
    print("deleting {}, time_stamp>{}, database={}".format(code, start_date, DATABASE))
    coll_stock_day.delete_many(myquery)
    print("delete done")


if __name__ == '__main__':
    delete_data_one_week("OSTK")
