import datetime
import os
import time
from concurrent.futures import ThreadPoolExecutor, wait, ALL_COMPLETED
from multiprocessing import Process
from multiprocessing import Pool

import pandas as pd

from QUANTAXIS import QA_Setting, QA_util_to_json_from_pandas, pymongo

QASETTING = QA_Setting()
DATABASE = QASETTING.client.quantaxis
stocks=[
    "ATI"
]
# stocks = [
# "A",
# "ATI",
# "UPS",
# "AKAM",
# "WWE",
# "BLK",
# "IVZ",
# "SRPT",
# "HSBC",
# "NVS",
# "JNPR",
# "SKX",
# "MKC",
# "GS",
# "GIL",
# "QQQ",
# "TSCO",
# "MTB",
# "EWBC",
# "XLI",
# "NVDA",
# "XLB",
# "XLE",
# "XLF",
# "XLK",
# "XLP",
# "XLU",
# "XLV",
# "XLY",
# "ONB",
# "ARE",
# "MCO",
# "PAA",
# "URBN",
# "EBAY",
# "CCI",
# "AU",
# "EPD",
# "RSG",
# "SRE",
# "CTSH"
# "SAP",
# "MANH",
# "APH",
# "OSK",
# "AMT",
# "PWR",
# "DIA",
# "AVB",
# "AEE",
# "BBT",
# "IP",
# "RIO",
# "SCHW",
# "CMA",
# "SNE",
# "CUZ",
# "SYY",
# "MAS",
# "TJX",
# "DRI",
# "EIX",
# "MGM",
# "AA",
# "MS",
# "ADM",
# "ALB",
# "FMC",
# "WEC",
# "WY",
# "GPK",
# "OXY",
# "XOM",
# "HD",
# "PNR",
# "BP",
# "PZZA",
# "CAH",
# "IPG",
# "RJF",
# "JBHT",
# "CMCSA",
# "CPB",
# "LPX",
# "T",
# "MCD",
# "DTE",
# "EL",
# "MHK",
# "UDR",
# "MSFT",
# "ADP",
# "NCR",
# "NSC",
# "AMTD",
# "WELL",
# "NYCB",
# "BHC",
# "HSIC",
# "PNW",
# "QCOM",
# "CAT",
# "CB",
# "RL",
# "JBL",
# "KIM",
# "SNPS",
# "CVS",
# "DGX",
# "TMO",
# "DUK",
# "UHS",
# "ETFC",
# "MSI",
# "NTAP",
# "WEN",
# "NYT",
# "ASML",
# "GPS",
# "PAAS"
# "AZN",
# "HES",
# "PG",
# "BHP",
# "HST",
# "IFF",
# "CHKP",
# "CMI",
# "SNV",
# "CPRT",
# "STLD",
# "CVX",
# "LRCX",
# "TAP",
# "DHI",
# "DVA",
# "TSM",
# "ELS",
# "MMC",
# "UL",
# "ETN",
# "MT",
# "ADSK",
# "NEE",
# "ALK",
# "AMZN",
# "WFC",
# "O",
# "BA",
# "XRAY",
# "PGR",
# "HSY",
# "BSX",
# "IGT",
# "CCEP",
# "IR",
# "ROK",
# "CHL",
# "JCI",
# "CMS"
# "KLAC",
# "CREE",
# "LEG",
# "STM",
# "DHR",
# "DVN",
# "TSN",
# "EMN",
# "MMM",
# "AAPL",
# "ETR",
# "FAST",
# "NEM",
# "ALL",
# "ANF",
# "WHR",
# "HIG",
# "PH",
# "PPG",
# "BTI",
# "IIVI",
# "CHRW",
# "KMB",
# "SO",
# "LEN",
# "CY",
# "LUV",
# "TOL",
# "DXC",
# "MCHP",
# "EMR",
# "UN",
# "AEM",
# "VFC",
# "VTR",
# "GIS",
# "OHI",
# "PAYX",
# "BAC",
# "XRX"
# "HIW",
# "PHG",
# "BK",
# "PPL",
# "CCK",
# "IRM",
# "ROST",
# "CNI",
# "LHX",
# "TD",
# "DIS",
# "TOT",
# "MCK",
# "MO",
# "UNH",
# "AEO",
# "AON",
# "WM",
# "OKE",
# "ATO",
# "GSK",
# "PBCT",
# "PHM",
# "HUM",
# "RCL",
# "CCL",
# "JNJ",
# "CNP",
# "KMT",
# "SPG",
# "CRUS",
# "LIN",
# "STT",
# "EA",
# "ABC",
# "UNM",
# "EVRG",
# "MTG",
# "AEP",
# "NI"
# "VZ",
# "NTRS",
# "AOS",
# "WMB",
# "GLW",
# "PII",
# "PSA",
# "BWA",
# "INCY",
# "RTN",
# "CI",
# "COF",
# "KMX",
# "SPGI",
# "D",
# "DISH",
# "MOS",
# "UNP",
# "AES",
# "FDX",
# "NKE",
# "ALXN",
# "NUAN",
# "APA",
# "WMT",
# "ATVI",
# "BAX",
# "YUM",
# "HOG",
# "REG",
# "CDNS",
# "ITW",
# "CIEN",
# "SKT",
# "COG",
# "CSCO",
# "STZ",
# "ECL",
# "EOG",
# "ABT",
# "MTZ"
# "AFL",
# "VLO",
# "FE",
# "AMAT",
# "WAB",
# "NUE",
# "APD",
# "OMC",
# "PCAR",
# "HOLX",
# "BLL",
# "BXP",
# "REGN",
# "RY",
# "CINF",
# "JPM",
# "KO",
# "CSX",
# "LLY",
# "SU",
# "M",
# "ED",
# "TXN",
# "EWJ",
# "MU",
# "VMC",
# "GNTX",
# "HON",
# "PTC",
# "BYD",
# "INTC",
# "RYAAY",
# "JWN",
# "SLB",
# "KR",
# "CTL",
# "LMT",
# "TER",
# "DLTR",
# "TROW",
# "URI"
# "MUR",
# "AGN",
# "FISV",
# "NNN",
# "AME",
# "NVO",
# "GOLD",
# "XLNX",
# "HAL",
# "BBY",
# "HP",
# "INTU",
# "RF",
# "CERN",
# "CL",
# "K",
# "SLG",
# "LNC",
# "SWK",
# "DD",
# "MAC",
# "TRP",
# "MDT",
# "TXT",
# "USB",
# "MXIM",
# "FITB",
# "NOC",
# "AMG",
# "WBA",
# "WSM",
# "ORCL",
# "HAS",
# "PEG",
# "BC",
# "PLD",
# "BMY",
# "PVH",
# "CLB",
# "KBH",
# "COP"
# "KSS",
# "SPY",
# "SWKS",
# "MAN",
# "TGT",
# "DOV",
# "MDY",
# "EQR",
# "MRK",
# "MYL",
# "AIG",
# "NOV",
# "AMGN",
# "WTR",
# "HBAN",
# "PEP",
# "BCE",
# "ZION",
# "HPQ",
# "IAC",
# "C",
# "COST",
# "KSU",
# "CTXS",
# "LNT",
# "DE",
# "THO",
# "TRV",
# "EFX",
# "MRO"
# "ADBE",
# "NBIX",
# "AJG",
# "VOD",
# "GD",
# "NWL",
# "BDX",
# "HRB",
# "IBM",
# "PXD",
# "RHI",
# "SBUX",
# "CLX",
# "KEY",
# "LB",
# "LOW",
# "SYK",
# "MAR",
# "TIF",
# "ES",
# "ADI",
# "UTX",
# "EXC",
# "NBL",
# "FL",
# "WDC",
# "GPC",
# "OTEX",
# "AXP",
# "PFE",
# "BEN",
# "HRL",
# "PNC",
# "CAG"
# ]

def change_time_format(time_, type):
    if type == 1:
        # yyyy-mm-dd格式
        # return time_.strftime('%Y-%m-%d')
        return datetime.datetime.strptime(time_, '%Y-%m-%d %H:%M:%S').strftime( '%Y-%m-%d')
    elif type == 2:
        # yyyy-mm-dd hh:mm格式
        # return time_.strftime('%Y-%m-%d %H:%M')
        return datetime.datetime.strptime(time_, '%Y-%m-%d %H:%M:%S').strftime('%Y-%m-%d %H:%M')
    elif type==3:
        # timestr = str(time_)[0:19]
        # return time_.strftime('%Y-%m-%d %H:%M:%S')
        return datetime.datetime.strptime(time_, '%Y-%m-%d %H:%M:%S').strftime('%Y-%m-%d %H:%M:%S')
    elif type == 4:
        return datetime.datetime.strptime(time_, '%Y-%m-%d %H:%M:%S').strftime('%H:%M:%S')

def util_time_stamp(time_):
    """
    explanation:
       转换日期时间的字符串为浮点数的时间戳

    params:
        * time_->
            含义: 日期时间
            类型: str
            参数支持: ['2018-01-01 00:00:00']

    return:
        time
    """
    if len(str(time_)) == 10:
        # yyyy-mm-dd格式
        return time.mktime(time.strptime(time_, '%Y-%m-%d'))
    elif len(str(time_)) == 16:
        # yyyy-mm-dd hh:mm格式
        return time.mktime(time.strptime(time_, '%Y-%m-%d %H:%M'))
    else:
        timestr = str(time_)[0:19]
        return time.mktime(time.strptime(timestr, '%Y-%m-%d %H:%M:%S'))

def QA_SU_save_firsttradedata_5min(file, client=QASETTING.client.quantaxis):
    coll = client.usstock_min
    coll.create_index(
        [
            ('code',
             pymongo.ASCENDING),
            ('time_stamp',
             pymongo.ASCENDING),
            ('date_stamp',
             pymongo.ASCENDING)
        ]
    )

    def __saving_work(coll, f):
        if f.__contains__(".txt"):
            path = os.path.join(root, f)
            code = f.split('_')[0]
            print("saving {}".format(code))
            data = pd.read_csv(path, names=['datetime', 'open', 'high', 'low', 'close', 'trade']);

            data = data.assign(tradetime=data['datetime'].apply(lambda x: change_time_format(x, type=2)),
                               date=data['datetime'].apply(lambda x: change_time_format(x, type=1)),
                               amount=0.0, position=0, price=round(0.0, 2), code=code,
                               type='5min'
                              )

            data = data.assign(date_stamp=data['date'].apply(lambda x: util_time_stamp(str(x))),
                               time_stamp=data['datetime'].apply(lambda x: util_time_stamp(str(x))),
                               )
            coll.insert_many(
                QA_util_to_json_from_pandas(data)
            )

    with ThreadPoolExecutor(max_workers=5) as t:

        for root, dirs, files in os.walk(file):
        # root 表示当前正在访问的文件夹路径
        # dirs 表示该文件夹下的子目录名list
        # files 表示该文件夹下的文件list
        # 遍历文件
            res = {
                t.submit(__saving_work, coll, f) for f in files
            }
            wait(res, return_when=ALL_COMPLETED)

def do_saving_1min_work(code, filepath):
    print("dealing file {}".format(filepath))

    if filepath.__contains__(".txt"):
        try:

            print("saving {}".format(code))
            data = pd.read_csv(filepath, names=['datetime', 'open', 'high', 'low', 'close', 'trade']);

            data = data.assign(tradetime=data['datetime'].apply(lambda x: change_time_format(x, type=2)),
                               date=data['datetime'].apply(lambda x: change_time_format(x, type=1)),
                               time=data['datetime'].apply(lambda x: change_time_format(x, type=4)),
                               amount=0.0, position=0, price=round(0.0, 2), code=code,
                               type='1min'
                              )

            data = data.assign(date_stamp=data['date'].apply(lambda x: util_time_stamp(str(x))),
                               time_stamp=data['datetime'].apply(lambda x: util_time_stamp(str(x))),
                               )
            data = data[(pd.to_datetime(data['time'], format='%H:%M:%S') >= pd.to_datetime('09:30:00',
                                                                                                   format='%H:%M:%S')) & (
                                        pd.to_datetime(data['time'], format='%H:%M:%S') <= pd.to_datetime(
                                    '16:00:00', format='%H:%M:%S'))]

            client = QASETTING.client
            coll = client.quantaxis.usstock_min
            coll.create_index(
                [
                    ('code',
                     pymongo.ASCENDING),
                    ('time_stamp',
                     pymongo.ASCENDING),
                    ('date_stamp',
                     pymongo.ASCENDING)
                ]
            )
            coll.delete_many({"code": code.upper(), "type": "1min"})
            coll.insert_many(
                QA_util_to_json_from_pandas(data)
            )
            client.close()
            print("saved {}".format(code))
        except Exception as e:
            print("ERROR {}, {}".format(code, e))

def QA_SU_save_firsttradedata_1min(root):
    # coll = client.usstock_min
    # coll.create_index(
    #     [
    #         ('code',
    #          pymongo.ASCENDING),
    #         ('time_stamp',
    #          pymongo.ASCENDING),
    #         ('date_stamp',
    #          pymongo.ASCENDING)
    #     ]
    # )



    record = []
    po=Pool(10)
    for code in iter(stocks):
        po.apply_async(do_saving_1min_work, args=(code.upper(), root + "/" + code.upper() + "_1min.txt"))
        # p = Process(target=__saving_work, args=(stock.upper(), root + "/"+stock.upper() + "_1min.txt"))
        # p.start()

        # record.append(p)

    po.close()
    po.join()

    # for p in record:
    #     p.join()
if __name__ == '__main__':
    QA_SU_save_firsttradedata_5min("/Users/gaikou/fund/data/")
    # QA_SU_save_firsttradedata_1min("/Users/gaikou/fund/quant/log/firsttradedemo")
    # QA_SU_save_firsttradedata_1min("/Users/gaikou/fund/data")
    # QA_SU_save_firsttradedata_1min("/mnt/log/stock_data/3000_Most_Liquid_US_Stocks/1min")