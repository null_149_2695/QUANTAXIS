import QUANTAXIS as QA


N = 40
mu = 1

import pandas as pd
def strategy001(data, N=40, mu=1):
    MP = QA.MA((data.high+data.low+data.close)/3, N)
    TR = pd.concat([abs(data.high - data.low), abs(data.high- data.close.shift(1)), abs(data.low - data.close.shift(1))],axis=1).max(axis=1)
    upBand = MP + mu*QA.MA(TR, N)
    dnBand = MP + mu*QA.MA(TR, 40)
    FP = MP
    return pd.DataFrame({'MP': MP, 'TR': TR, 'upBand': upBand, 'dnBand':dnBand, 'FP':MP})

if __name__ == '__main__':
    data = QA.QA_fetch_usstock_day_adv("ALL", '2020-01-01', '2021-09-10')
    data.show()
    ind = data.add_func(strategy001)
    print(ind.tail)
    MPDIFF = ind.MP.diff().dropna()

    user = QA.QA_User(username='quantaxiss', password='quantaxis')
    portfolio = user.new_portfolio('strategy101')

    acc = portfolio.new_account(account_cookie='acc001', init_hold={'RBL8': 0}, init_cash=30000,
                                market_type=QA.MARKET_TYPE.STOCK_US)
    lastprice = 0

    for idx, item in data.iterrows():
        # try:
        if acc.hold_available.get(idx[1], 0) == 0 and MPDIFF.loc[idx] > 0 and item['close'] > ind.upBand.loc[idx]:
            print('buyOPEN _ {}'.format(idx))
            acc.receive_simpledeal(
                code=idx[1],
                trade_price=item['close'],
                trade_amount=1,
                trade_towards=QA.ORDER_DIRECTION.BUY_OPEN,
                trade_time=idx[0])
        if acc.hold_available.get(idx[1], 0) == 0 and MPDIFF.loc[idx] < 0 and item['close'] < ind.upBand.loc[idx]:
            print('sellOPEN_ {}'.format(idx))
            acc.receive_simpledeal(
                code=idx[1],
                trade_price=item['close'],
                trade_amount=1,
                trade_towards=QA.ORDER_DIRECTION.SELL_OPEN,
                trade_time=idx[0])
        if lastprice < ind.FP.loc[idx] and item['close'] > ind.FP.loc[idx]:
            print('close')
            if acc.hold_available.get(idx[1], 0) > 0:
                # 多单止盈
                acc.receive_simpledeal(
                    code=idx[1],
                    trade_price=item['close'],
                    trade_amount=1,
                    trade_towards=QA.ORDER_DIRECTION.SELL_CLOSE,
                    trade_time=idx[0])
            elif acc.hold_available.get(idx[1], 0) < 0:
                # 空单止损
                acc.receive_simpledeal(
                    code=idx[1],
                    trade_price=item['close'],
                    trade_amount=1,
                    trade_towards=QA.ORDER_DIRECTION.BUY_CLOSE,
                    trade_time=idx[0])

        if lastprice > ind.FP.loc[idx] and item['close'] < ind.FP.loc[idx]:
            print('close')
            if acc.hold_available.get(idx[1], 0) > 0:
                # 多单止损
                acc.receive_simpledeal(
                    code=idx[1],
                    trade_price=item['close'],
                    trade_amount=1,
                    trade_towards=QA.ORDER_DIRECTION.SELL_CLOSE,
                    trade_time=idx[0])
            elif acc.hold_available.get(idx[1], 0) < 0:
                # 空单止盈
                acc.receive_simpledeal(
                    code=idx[1],
                    trade_price=item['close'],
                    trade_amount=1,
                    trade_towards=QA.ORDER_DIRECTION.BUY_CLOSE,
                    trade_time=idx[0])
        # except:
        #     pass
        lastprice = item['close']

        acc.history_table

        performance = QA.QA_Performance(acc)

        performance.pnl_fifo

        acc.market_type

